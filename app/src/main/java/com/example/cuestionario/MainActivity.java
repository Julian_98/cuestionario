package com.example.cuestionario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    //Declaracion de variables.
    CheckBox chek1, chek2, chek3, chek4, chek5, chek6, chek7, chek8, chek9, chek10, chek11, chek12, chek13, chek14, chek15, chek16, chek17, chek18, chek19;
    RadioButton radio1, radio2, radio3, radio4, radio5, radio6, radio7, radio8, radio9, radio10, radio11, radio12, radio13, radio14, radio15;
    ToggleButton toggle1, toggle2;
    EditText txtRespuesta;
    TextView tView;
    double v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20,
            v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Permite obtener el elemento desde el XML
        chek1=findViewById(R.id.chek1);
        chek2=findViewById(R.id.chek2);
        chek3=findViewById(R.id.chek3);
        chek4=findViewById(R.id.chek4);
        chek5=findViewById(R.id.chek5);
        chek6=findViewById(R.id.chek6);
        chek7=findViewById(R.id.chek7);
        chek8=findViewById(R.id.chek8);
        chek9=findViewById(R.id.chek9);
        chek10=findViewById(R.id.chek10);
        chek11=findViewById(R.id.chek11);
        chek12=findViewById(R.id.chek12);
        chek13=findViewById(R.id.chek13);
        chek14=findViewById(R.id.chek14);
        chek15=findViewById(R.id.chek15);
        chek16=findViewById(R.id.chek16);
        chek17=findViewById(R.id.chek17);
        chek18=findViewById(R.id.chek18);
        chek19=findViewById(R.id.chek19);
        radio1=findViewById(R.id.radio1);
        radio2=findViewById(R.id.radio2);
        radio3=findViewById(R.id.radio3);
        radio4=findViewById(R.id.radio4);
        radio5=findViewById(R.id.radio5);
        radio6=findViewById(R.id.radio6);
        radio7=findViewById(R.id.radio7);
        radio8=findViewById(R.id.radio8);
        radio9=findViewById(R.id.radio9);
        radio10=findViewById(R.id.radio10);
        radio11=findViewById(R.id.radio11);
        radio12=findViewById(R.id.radio12);
        radio13=findViewById(R.id.radio13);
        radio14=findViewById(R.id.radio14);
        radio15=findViewById(R.id.radio15);
        toggle1=findViewById(R.id.toggle1);
        toggle2=findViewById(R.id.toggle2);
        txtRespuesta=findViewById(R.id.txtRespuesta);
        tView=findViewById(R.id.txtCalificacion);
        toggle1.setOnCheckedChangeListener(this);
        toggle2.setOnCheckedChangeListener(this);
    }

    public void verificarValores(View view) {
        //Metodo verificar datos para asignar el valor correspondiente a cada pregunta mediante ternario.
        v1 = chek1.isChecked() ? 0.5 : 0;
        v2 = chek2.isChecked() ? 0.0 : 0;
        v3 = chek3.isClickable() ? 0.0 : 0;
        v4 = chek4.isClickable() ? 0.5 : 0;
        v5 = chek5.isClickable() ? 0.0 : 0;
        v6 = chek6.isClickable() ? 0.5 : 0;
        v7 = radio1.isChecked() ? 0.5 : 0;
        v8 = radio2.isChecked() ? 0.0 : 0;
        v9 = radio3.isChecked() ? 0.5 : 0;
        v10 = radio4.isChecked() ? 0.0 : 0;
        v11 = radio5.isChecked() ? 0.0 : 0;
        v12 = radio6.isChecked() ? 0.5 : 0;
        v13 = chek7.isChecked() ? 0.0 : 0;
        v14 = radio7.isChecked() ? 0.5 : 0;
        v15 = radio8.isChecked() ? 0.5 : 0;
        v16 = chek8.isChecked() ? 0.0 : 0;
        v17 = chek9.isChecked() ? 0.0 : 0;
        v18 = chek10.isChecked() ? 0.5 : 0;
        v19 = radio9.isChecked() ? 0.5 : 0;
        v20 = radio10.isChecked() ? 0.0 : 0;
        v21 = chek11.isChecked() ? 0.5 : 0;
        v22 = chek12.isChecked() ? 0.0 : 0;
        v23=toggle1.isChecked() ? 0.5 : 0;
        v24=toggle2.isChecked() ? 0.5 : 0;
        v25=chek14.isChecked() ? 1.0 : 0;
        v26=chek15.isChecked() ? 0.0 : 0;
        v27=chek16.isChecked() ? 0.5 : 0;
        v28=radio11.isChecked() ? 0.5 : 0;
        v29=radio12.isChecked() ? 0.0 : 0;
        v30=radio14.isChecked() ? 0.5 : 0;
        v31=radio15.isChecked() ? 0.5 : 0;
        v32=chek17.isChecked() ? 0.0 : 0;
        v33=chek18.isChecked() ? 0.0 : 0;
        v34=chek19.isChecked() ? 0.5 : 0;
        v35 = chek13.isChecked() ? 0.0 : 0;

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public void calificar(View view) {
        //Metodo que hace la evaluacion de condiciones para asignar calificacion.
        if( (chek1.isChecked() || chek2.isChecked()) && (chek3.isChecked() || chek4.isChecked()) && (chek5.isChecked() || chek6.isChecked()) && (radio1.isChecked() || radio2.isChecked()) && (radio3.isChecked() || radio4.isChecked()) && (radio5.isChecked() || radio6.isChecked()) && (chek7.isChecked() || radio7.isChecked()) && (radio8.isChecked() || chek8.isChecked()) && (chek9.isChecked() || chek10.isChecked()) &&
                (radio9.isChecked() || radio10.isChecked()) && (chek11.isChecked() || chek12.isChecked()) && (toggle1.isChecked()) && (toggle2.isChecked()) && (chek13.isChecked() || chek14.isChecked()) && (chek15.isChecked() || chek16.isChecked()) && (radio11.isChecked() || chek12.isChecked()) && (radio13.isChecked() || radio14.isChecked()) && (radio15.isChecked() || chek17.isChecked()) && (chek18.isChecked() || chek19.isChecked())   )
        {
            //Suma de todos los valores asignados en el Metodo verificarValores
            total=v1+v4+v6+v7+v9+v12+v14+v15+v18+v19+v21+v23+v24+v25+v27+v28+v30+v31+v34;
         tView.setText("Tu Calificacion es " + String.valueOf(total));
        }
        else
        {
            String mensaje="Debes Contestar Todas las Preguntas";
            Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show();

        }

        //||
    }
}
